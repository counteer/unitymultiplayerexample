﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Health : 	NetworkBehaviour 
{
	public const int maxHealth = 100;

	public bool destroyOnDeath;

	[SyncVar (hook="OnHealthChange")]
	public int currentHealth = maxHealth;

	public RectTransform healthBar;



	public void TakeDamage(int amount)
	{
		if (!isServer) {
			return;
		}
		currentHealth -= amount;
		if (currentHealth <= 0)
		{
			if (destroyOnDeath) {
				Destroy (gameObject);
			} else {
				currentHealth = maxHealth;
				RpcRespawn ();
			}
		}
	}

	void OnHealthChange(int currentHealth){
		healthBar.sizeDelta = new Vector2 (currentHealth, healthBar.sizeDelta.y);
	}

	[ClientRpc]
	void RpcRespawn()
	{
		if (isLocalPlayer)
		{
			// move back to zero location
			transform.position = Vector3.zero;
		}
	}
}